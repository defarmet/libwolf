.POSIX:
.SUFFIXES:
CC = cc
CFLAGS = -march=native -std=c11 -Wpedantic -O3 -fPIC -Wall -Wextra -Werror

all: obj
obj: libwolf.c libwolf.h
	$(CC) $(CFLAGS) -shared -c libwolf.c -o libwolf.so

test: libwolf.c libwolf.h test.c
	$(CC) $(CFLAGS) -o test test.c libwolf.c
	./test

clean:
	rm -f *.*o test
