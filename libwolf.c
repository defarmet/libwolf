/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "libwolf.h"

void add_to_team(struct Game *game, char num, short hole, short team)
{
	if (!game)
		return;

	if (team == W && game->sizes[W][hole] >= game->count / 2)
		return;

	if (team == H && game->sizes[H][hole] >= game->count - 1)
		return;

	game->teams[team][hole][game->sizes[team][hole]] = num;
	game->sizes[team][hole]++;
}

long get_earnings(struct Game game, char num, short hole)
{
	long earnings = 0;
	for (short i = 0; i <= hole; i++) {
		long amount = game.amounts[i];
		long w_amount = amount;
		if (game.scores[W][i] > game.scores[H][i])
			amount = -amount;
		else if (game.scores[W][i] == game.scores[H][i])
			amount = 0;

		if (game.sizes[W][i] != 0)
			w_amount = amount * game.sizes[H][i] / game.sizes[W][i];

		for (short j = 0; j < game.sizes[W][i]; j++) {
			if (game.teams[W][i][j] == num)
				earnings += w_amount;
		}

		for (short j = 0; j < game.sizes[H][i]; j++) {
			if (game.teams[H][i][j] == num)
				earnings -= amount;
		}
	}

	return earnings;
}

char get_wolf(struct Game game, short hole)
{
	if (18 - hole <= (18 % game.count ? 18 % game.count :
			game.count == 18 ? 0 : game.count)) {
		char low = 0;
		long low_earnings = get_earnings(game, 0, hole);

		for (char i = 1; i < game.count; i++) {
			long earnings = get_earnings(game, i, hole);
			if (earnings < low_earnings) {
				low = i;
				low_earnings = earnings;
			}
		}

		return low;
	}

	return hole % game.count;
}
