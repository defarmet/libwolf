/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "libwolf.h"

int main(void)
{
	time_t t;
	srand((unsigned)time(&t));

	struct Game game = {0};
	game.count = 18;
	for (short i = 0; i < 18; i++) {
		char wolf = get_wolf(game, i);
		add_to_team(&game, wolf, i, W);

		for (short j = wolf + 1; j != wolf; j = (j + 1) % game.count) {
			add_to_team(&game, j, i, H);
		}

		game.amounts[i] = 6;

		game.scores[W][i] = rand() % 3 + 3;
		game.scores[H][i] = rand() % 3 + 3;

		printf("Wolves: ");
		for (short j = 0; j < game.sizes[W][i]; j++)
			printf("%d ", game.teams[W][i][j]);

		printf("\nHunters: ");
		for (short j = 0; j < game.sizes[H][i]; j++)
			printf("%d ", game.teams[H][i][j]);

		printf("\nScores: %d %d\n",
				game.scores[W][i], game.scores[H][i]);

		for (short j = 0; j < game.count; j++)
			printf("Player %2d earnings: %ld\n",
					j, get_earnings(game, j, i));

		printf("\n\n");
	}

	return 0;
}
