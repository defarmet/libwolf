/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef LIBWOLF_H
#define LIBWOLF_H

#define MAX_PLAYERS 18

#define W 0
#define H 1

struct Game {
	unsigned char count;
	unsigned char scores[2][18];
	unsigned char sizes[2][18];
	char teams[2][18][MAX_PLAYERS - 1];
	long amounts[18];
};

void add_to_team(struct Game *game, char num, short hole, short team);
long get_earnings(struct Game game, char num, short hole);
char get_wolf(struct Game game, short hole);

#endif
