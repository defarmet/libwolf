# libwolf
A library for the Wolf golf betting game. This library allows for easy player and earnings tracking for a separate interface.

## How the Game Works
Wolf is an at least 4 player golf betting game. The game begins by deciding the player order. The first player becomes the "wolf" and goes first. The rest of the players then make their first shot. If the wolf likes a person's shot, then that player joins the wolf's team. This action must be done before the next player makes their shot. If the wolf does not choose anyone, the bet is doubled. Each team then takes the lowest number of shots for the hole. For example, if the wolf shoots a 5 while the wolf's teammate shoots a 4, then the wolf team's sore for the hole is 4. The same would apply to the other team (the "hunters"). Once the winner is decided. The next person in the order becomes the wolf and the process repeats through the order of players and loops back to the original wolf. During the final holes, adjusted for the number of players, the person who is losing the most becomes the wolf. This is updated for each hole. For example, in a group of 4, holes 17 and 18 would have the loser be the wolf. In a group of 6 players, the last 6 holes will use the loser calculation.  

### Extra Rules
The wolf can decide to go alone before anyone takes a shot. The bet is tripled.  
If there is a tie on a hole, the bet from that hole can transfer to the next hole's bet.  
In a group of 6 or more, after all of the first shots are taken, the first player not chosen by the wolf (usually the player after the wolf unless that player was chosen) can send 1 or more players to the wolf's team. The wolf can either refuse or take all members.  
The player chosen by the wolf can refuse and instead become the wolf. The wolf cannot have any extra players on the team. The next wolf does not change and the player order remains the same.  

## API
Player names are to be managed in the program, not this library. Any functions requiring a player will use a number, set by the player order. The maximum number of players is 18. The hole number begins at 0 and ends at 17.  
  

`W`, `H`  
	Constants indicating the teams. Use the constants to access team-specific data values. 

`void add_to_team(struct Game *game, char num, short hole, short team)`  
	Adds a player, indicated by a number, to a team (`W` or `H`) for a given hole.  

`long get_earnings(struct Game game, char num, short hole)`  
	Takes a player number and returns the earnings of that player for a given hole.  

`char get_wolf(struct Game game, short hole)`  
	Takes a hole number and returns the corresponding wolf.  

## Licensing
(c) 2020-2022 defarmet defarmet@protonmail.com
